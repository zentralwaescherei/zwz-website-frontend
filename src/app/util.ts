type Dimensions = { width: number; height: number }

export function getWindowDimensions(): Dimensions {
	const { innerWidth: width, innerHeight: height } = window
	return {
		width,
		height,
	}
}
