import { configureStore } from '@reduxjs/toolkit'
import { counterReducer } from '../features/counter/counterSlice'
import { languageReducer } from '../features/language/languageSlice'
import { setupListeners } from '@reduxjs/toolkit/query'
import { eventsApi } from '../services/events-service'
import { joinReducer } from '../features/join/joinSlice'
import { awarnessApi } from '../services/awarness-service'
import { eventReducer } from '../features/events/eventSlice'
import { liveApi } from '../services/live-service'

export const store = configureStore({
	reducer: {
		language: languageReducer,
		counter: counterReducer,
		join: joinReducer,
		event: eventReducer,
		[eventsApi.reducerPath]: eventsApi.reducer,
		[awarnessApi.reducerPath]: awarnessApi.reducer,
		[liveApi.reducerPath]: liveApi.reducer,
	},
	middleware: (getDefaultMiddleware) =>
		getDefaultMiddleware().concat(eventsApi.middleware).concat(awarnessApi.middleware).concat(liveApi.middleware),
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch

setupListeners(store.dispatch)
