import * as React from 'react'
import { GastroMenu } from '../gastro-menu/gastro-menu'

export const Menu = (): JSX.Element => {
	return (
		<div className="basic-paragraph">
			<GastroMenu />
		</div>
	)
}
