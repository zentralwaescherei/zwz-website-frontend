import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { Markdown } from '../markdown/markdown'

export const GastroMenu = (): JSX.Element => {
	const { t } = useTranslation('gastro')

	return (
		<div>
			<div className="gastro-menu-tile menu-info">
				<p className="center-menu">
					<Markdown>{t('gastro.gastro-info')}</Markdown>
				</p>
			</div>
			<div className="gastro-menu-tile menu-info">
				<img src="https://drive.google.com/uc?export=view&id=1b4FzT1_2EIg2yY6Uk-pez7ncEPaiETKg"></img>
			</div>
		</div>
	)
}
