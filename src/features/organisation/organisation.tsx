import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { Markdown } from '../markdown/markdown'

export const Organisation = (): JSX.Element => {
	const { t } = useTranslation('organisation')
	return (
		<div className="basic-paragraph">
			<Markdown>{t('organisation.content')}</Markdown>
		</div>
	)
}
