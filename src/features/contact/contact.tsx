import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { Markdown } from '../markdown/markdown'

export const Contact = (): JSX.Element => {
	const { t } = useTranslation('contact')
	return (
		<div className="basic-paragraph">
			<Markdown>{t('contact.content')}</Markdown>
		</div>
	)
}
