import * as React from 'react'

export const Live = (): JSX.Element => {
	return (
		<div className="basic-paragraph">
			<div className="live-container">
				<iframe
					className="live-responsive-iframe"
					src="https://player.twitch.tv/?channel=zentralwaescherei&parent=zentralwaescherei.space"
					frameBorder="0"
					scrolling="no"
					allowFullScreen
				></iframe>
			</div>
		</div>
	)
}
