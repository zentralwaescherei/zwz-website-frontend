import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { Markdown } from '../markdown/markdown'

export const Jobs = (): JSX.Element => {
	const { t } = useTranslation('jobs')
	return (
		<div className="basic-paragraph">
			<div className="jobs-img">
				<Markdown>{t('jobs.content')}</Markdown>
			</div>
		</div>
	)
}
