import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { Markdown } from '../markdown/markdown'

export const About = (): JSX.Element => {
	const { t } = useTranslation('about')
	return (
		<div className="basic-paragraph">
			<Markdown>{t('about.content')}</Markdown>
		</div>
	)
}
