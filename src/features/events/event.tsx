import * as React from 'react'
import { format, fromUnixTime } from 'date-fns/fp'
import { useEffect, useState } from 'react'
import { getWindowDimensions } from '../../app/util'
import { PartialEvent } from '../../services/events.routes.d'
import { EventDetail } from './event-detail'
import { useAppDispatch, useAppSelector } from '../../app/hooks'
import { selectEventUiState, setEventUiState } from './eventSlice'

const formatDate = (date: number) => format('dd.MM.yy')(fromUnixTime(date))
const formatTime = (date: number) => format('HH:mm')(fromUnixTime(date))

type EventProps = { event: PartialEvent; alwaysExpanded?: boolean; hideShareLink?: boolean }

export const Event = ({ event, alwaysExpanded, hideShareLink }: EventProps): JSX.Element => {
	const dispatch = useAppDispatch()

	const [hover, setHover] = useState(false)
	const [isMobile, setIsMobile] = useState(false)
	const [windowDimensions] = useState(getWindowDimensions())
	const eventUiState = useAppSelector(selectEventUiState(event.id || '')) || { expanded: alwaysExpanded || false }

	useEffect(() => {
		dispatch(
			setEventUiState({
				id: event.id!,
				expanded: !!alwaysExpanded,
			})
		)
	}, [])

	useEffect(() => {
		if (windowDimensions.width < 800) {
			setIsMobile(true)
		}
	}, [windowDimensions])

	const toggle = () => {
		if (!isMobile && !alwaysExpanded) {
			dispatch(
				setEventUiState({
					id: event.id || '',
					expanded: !eventUiState.expanded,
				})
			)
		}
	}

	const date = () => <span className="event-date">{formatDate(event.fromDate)}</span>
	const time = () => <span className="event-time">{formatTime(event.fromDate)}</span>
	const title = () => <span className="event-title">{event.title || 'title'}</span>
	const location = () => <span className="event-location">{event.location.join(', ') || 'location'}</span>
	const type = () => <span className="event-type">{event.type || 'type'}</span>

	const content = () => {
		if (isMobile) {
			return (
				<>
					{date()}
					{type()}
					{location()}
				</>
			)
		} else {
			return (
				<>
					{date()}
					{time()}
					{title()}
					{location()}
					{type()}
					<span></span>
				</>
			)
		}
	}

	const mobileSubheader = () => (
		<div className="event-line-mobile">
			{title()}
			{time()}
			<span className="event-ghost"></span>
		</div>
	)

	return (
		<div
			className={
				'event' +
				(eventUiState.expanded || alwaysExpanded ? ' active' : '') +
				(alwaysExpanded ? ' always-expanded' : '')
			}
			onClick={toggle}
			onMouseEnter={() => setHover(true)}
			onMouseLeave={() => setHover(false)}
		>
			<div className="event-line">{content()}</div>
			{isMobile && mobileSubheader()}
			<EventDetail
				shown={eventUiState.expanded || isMobile}
				hover={hover}
				event={event}
				hideShareLink={hideShareLink}
			></EventDetail>
		</div>
	)
}
