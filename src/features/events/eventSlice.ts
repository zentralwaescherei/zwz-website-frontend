import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import type { RootState } from '../../app/store'
import { AnyAction, Reducer } from 'redux'
import { getUnixTime } from 'date-fns'

interface IDictionary {
	[index: string]: EventUiState
}
// Define a type for the slice state
interface EventState {
	from: number
	to: number
	eventsUiState: IDictionary
}

interface Filter {
	from: number
	to: number
}

interface EventUiPayloadAction extends EventUiState {
	id: string
}

interface EventUiState {
	expanded: boolean
}

// Define the initial state using that type
const initialState = {
	active: false,
	from: getUnixTime(Date.now()),
	to: 0,
	eventsUiState: {} as IDictionary,
} as EventState

export const eventSlice = createSlice({
	name: 'event',
	// `createSlice` will infer the state type from the `initialState` argument
	initialState,
	reducers: {
		filter: (state, action: PayloadAction<Filter>) => {
			state.from = action.payload.from
			state.to = action.payload.to
		},
		disableFilter: (state) => {
			state.from = 0
			state.to = 0
		},
		setEventUiState: (state, action: PayloadAction<EventUiPayloadAction>) => {
			const clone = Object.assign({} as IDictionary, state.eventsUiState)
			clone[action.payload.id] = { expanded: action.payload.expanded }
			state.eventsUiState = clone
		},
	},
})

export const { filter, disableFilter, setEventUiState } = eventSlice.actions

// Other code such as selectors can use the imported `RootState` type

export const eventReducer: Reducer<EventState, AnyAction> = eventSlice.reducer

export const selectFrom: (state: RootState) => number = (state: RootState) => state.event.from
export const selectTo: (state: RootState) => number = (state: RootState) => state.event.to
export function selectEventUiState(id: string): (state: RootState) => EventUiState | undefined {
	return (state: RootState) => state.event.eventsUiState[id]
}
