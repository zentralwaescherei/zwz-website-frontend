import * as React from 'react'
import { useAppSelector, useAppDispatch } from '../../app/hooks'
import { increment, decrement, selectCount } from './counterSlice'

export const Counter = (): JSX.Element => {
	const count = useAppSelector(selectCount)
	const dispatch = useAppDispatch()
	return (
		<div>
			<div>
				<button className="zw-button" aria-label="Increment value" onClick={() => dispatch(increment())}>
					MINUS
				</button>
				<span>Counter: {count}</span>
				<button className="zw-button" aria-label="Decrement value" onClick={() => dispatch(decrement())}>
					Decrement
				</button>
			</div>
		</div>
	)
}
