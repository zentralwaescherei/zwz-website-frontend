import { useFormik } from 'formik'
import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { useCreateJoinRequestMutation } from '../../services/join-service'
import { JoinRequest } from '../../services/join.routes'

export const JoinForm = (): JSX.Element => {
	const { t } = useTranslation('forms')

	const [createJoinRequest, { isLoading: isUpdating }] = useCreateJoinRequestMutation()

	const formik = useFormik<JoinRequest>({
		initialValues: {
			email: '',
			name: '',
			message: '',
		},
		onSubmit: (values, { resetForm, setSubmitting }) => {
			setSubmitting(true)

			if (!isUpdating) {
				createJoinRequest(values)
					.unwrap()
					.then((fulfilled) => console.log(fulfilled))
					.catch((rejected) => console.log(rejected))
			}

			setTimeout(() => {
				setSubmitting(false)
				resetForm()
			}, 800)
		},
	})

	return (
		<div>
			<form onSubmit={formik.handleSubmit}>
				<input
					className="lineInput"
					id="name"
					name="name"
					type="text"
					autoComplete="off"
					onChange={formik.handleChange}
					value={formik.values.name}
					placeholder={t('form.name')}
					required
				/>
				<input
					className="lineInput"
					id="email"
					name="email"
					type="email"
					autoComplete="off"
					onChange={formik.handleChange}
					value={formik.values.email}
					placeholder={t('form.email')}
					required
				/>
				<textarea
					className="lineInput"
					id="message"
					name="message"
					autoComplete="off"
					rows={3}
					required
					onChange={formik.handleChange}
					value={formik.values.message}
					placeholder={t('form.message')}
				/>
				<button className="button" type="submit" disabled={formik.isSubmitting}>
					{t('form.submit')}
				</button>
			</form>
		</div>
	)
}
