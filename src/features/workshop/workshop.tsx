import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { Markdown } from '../markdown/markdown'

export const Workshop = (): JSX.Element => {
	const { t } = useTranslation('workshop')
	return (
		<div className="basic-paragraph">
			<Markdown>{t('workshop.content')}</Markdown>
		</div>
	)
}
