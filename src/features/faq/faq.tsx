import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { Markdown } from '../markdown/markdown'

export const Faq = (): JSX.Element => {
	const { t } = useTranslation('faq')
	return (
		<div className="basic-paragraph">
			<Markdown>{t('faq.content')}</Markdown>
		</div>
	)
}
