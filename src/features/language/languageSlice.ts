import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import type { RootState } from '../../app/store'
import { AnyAction, Reducer } from 'redux'

//import { AnyAction, Reducer } from 'redux'

export type Language = 'en' | 'de'

// Define a type for the slice state
interface LanguageState {
	value: Language
}

// Define the initial state using that type
const initialState = { value: 'de' } as LanguageState

export const languageSlice = createSlice({
	name: 'language',
	// `createSlice` will infer the state type from the `initialState` argument
	initialState,
	reducers: {
		// Use the PayloadAction type to declare the contents of `action.payload`
		changeLanguage: (state, action: PayloadAction<Language>) => {
			state.value = action.payload
		},
	},
})

export const { changeLanguage } = languageSlice.actions

// Other code such as selectors can use the imported `RootState` type

//export const counterReducer: Reducer<CounterState, AnyAction> = counterSlice.reducer
export const languageReducer: Reducer<LanguageState, AnyAction> = languageSlice.reducer

export const selectLanguage: (state: RootState) => Language = (state: RootState) => state.language.value
