import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { Markdown } from '../markdown/markdown'

export const Covid = (): JSX.Element => {
	const { t } = useTranslation('covid')

	return (
		<div className="basic-paragraph">
			<Markdown>{t('covid.content')}</Markdown>
		</div>
	)
}
