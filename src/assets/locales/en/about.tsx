export default {
	'about.content': `We are the association Zentralwäscherei and run the non-commercial cultural and meeting space Zentralwäscherei in Kreis 5. We strive for a space for meeting, art, culture and gastronomy that is as inclusive as possible. A space for discussion, for publicity, for consumption, for dance, to present ourselves, to celebrate life, to question and to have fun.

---

We believe that culture should be accessible to as many people as possible. Based on this conviction, we offer guide prices for admission to cultural events. We understand guide prices to be recommended prices from which deviations are possible. This is especially to make it possible for people with precarious financial circumstances to attend the events. Since culture has its value and we can continue to exist as an uncommercial cultural enterprise, we are, however, also dependent on reasonable entrance fees.

---

Our operation includes the following spaces:

**ZW-K** The small laundry is our catering operation. 

**ZW-H** The large hall is our event space, the place for various event formats such as panel discussions, theater, performances and so on. 
	
**ZW-B** The sound room provides space for music events such as concerts, raves and sound performances. 
	
**ZW-W** Our workspace has a screen printing workshop and a sewing workshop. A woodworking workshop is in the planning stage. These can be used by all after an introductory course.

---

- The association is a growing collective of about 150 members at the moment. 
- Would you like to get involved? 
- Our "we" is always open to newcomers!`,
}
