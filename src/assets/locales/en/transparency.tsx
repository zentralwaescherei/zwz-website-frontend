export default {
	'transparency.content': `We strive to keep all aspects of the club as public and transparent as possible. On this page you will find an insight into many areas of the association. If you would like to learn more, please register as a new member.
---
---
- [Statutes of the association](https://drive.google.com/file/d/1NeLW_cLsAVfm1GFMcxcb-C3xNxp5NEg2/view?usp=sharing)
- Here you can download statutes.
---
- [Service agreement with the city of Zürich](https://drive.google.com/file/d/1ak_8XvJy9phHKY6lXNIQvgQdv5o3xl3_/view?usp=sharing)
- Here you can download our service agreement.
---
- [Application of the association for these premises](https://drive.google.com/file/d/1wL9AOTWOnD0QZlq2VZZq__UZz_QvNmg7/view?usp=sharing)
- Here you can find our application dossier.
---
- [Curation guidelines](https://docs.google.com/document/d/1B9hpZNoLfe9l17VjccezID-IeZKqeRWNfXvuKRLUB_o/edit#heading=h.boxkmdjs9i0b)
- Here you can download the curation guidelines.`,
}
