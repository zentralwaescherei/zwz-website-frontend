export default {
	'filter.allEvents': 'all events',
	'filter.thisWeek': 'this week',
	'filter.archive': 'archive',
}
