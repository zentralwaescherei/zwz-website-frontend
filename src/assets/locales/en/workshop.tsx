export default {
	'workshop.content': `- Our goal is to create together a self-organized workshop as accessible as possible.
- It is accessible to all people with a respective introductory course.
- All the people who have free access to the workshops take care of the space. These people together form a group in which they look after the infrastructure and each other.
---
- **If you are interested in a collaboraion**
- [naehen@zentralwaescherei.space](mailto:naehen@zentralwaescherei.space)
- [siebdrucken@zentralwaescherei.space](mailto:siebdrucken@zentralwaescherei.space)
---
- If you don't want to be part of the group, but want to use the workshop once, you can also write an email & we'll see what we can do.`,
}
