export default {
	'contact.content': `Feel free to contact us via mail if you have any questions. Suggestions and feedbacks are always welcome!

- General inquiries to the association please send to:
- [info@zentralwaescherei.space](mailto:info@zentralwaescherei.space)
---	
- You want to actively participate in the association? Then please contact us here:
- [mitmachen@zentralwaescherei.space](mailto:mitmachen@zentralwaescherei.space)
---
- For inquiries regarding programming please contact here:
- [programm@zentralwaescherei.space](mailto:programm@zentralwaescherei.space)
---
- For inquiries regarding gastronomy:
- [gastro@zentralwaescherei.space](mailto:gastro@zentralwaescherei.space)
---
- Verein Zentralwäscherei
- Neue Hard 12
- 8005 Zürich`,
}
