export default {
	'host.content': `In addition to its own events, the Zentralwäscherei association also wants to actively enter into co-productions with other organizations and collectives. Examples of event co-productions can be exhibitions, social formats, discursive formats, political events, concerts, theater, installations, safe spaces and sound performances. Excluded are events that violate our principles, private events or parties.

Four times a year we set deadlines for you to submit your projects. **AT THE MOMENT THE OPEN CALL IS CLOSED**.
	
We will then charge you a cooperation fee, which will be based on a variety of criteria such as use of space, use of infrastructure, days of the week, eligibility and a share of variable costs. Only days of performances and not rehearsals/setups (for exhibitions only opening days/final days) will be charged. The amount is between 50-500 CHF per day, depending on the rental key. The calculation basis for the cooperation fee can be found [HERE](https://docs.google.com/spreadsheets/d/1gPdp_VA5fN9x6iogIXWDp7VauhSm9xeupR6uA649_FU/edit?usp=sharing). 
	
No previous experience is necessary! Any level of experience is welcome!
	
If you are interested, you can read our principles and then fill out the questionnaire below. Submit a project description and if possible a budget plan.

We will get an overview of the projects after the deadline and decide on the projects in a planning week. You will receive feedback from us by the end of January. The guidelines, which form the basis of our decision, can be found [here](https://docs.google.com/document/d/1qi86Ok7bYnqdsOi6ARaKFe8I_MJABOVZJ9ZSk3GoH50/edit?usp=sharing).
	
If you have any further questions, please contact [programm@zentralwaescherei.space](mailto:programm@zentralwaescherei.space).
	
By registering, the organizer agrees to our [principles](/principles).`,
}
