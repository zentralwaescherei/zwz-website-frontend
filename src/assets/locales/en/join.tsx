export default {
	main: `Interested people who would like to get more deeply involved in the operation of the hall can apply as new members of the association. Please fill out the non-binding application form or write an email to: *[mitmachen@zentralwaescherei.space](mailto:mitmachen@zentralwaescherei.space)*`,
	button: 'JOIN',
	buttonClose: 'CLOSE',
}
