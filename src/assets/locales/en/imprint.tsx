export default {
	'imprint.content': `- Verein Zentralwäscherei
- Neue Hard 12
- 8005 Zürich
---
- [admin@zentralwäscherei.space](mailto:admin@zentralwäscherei.space)
---
---
**Disclaimer**

The Verein Zentralwäscherei assumes no liability whatsoever with regard to the correctness, accuracy, up-to-dateness, reliability and completeness of the information provided.
	
Liability claims regarding damage caused by the use of any information provided, including any kind of information which is incomplete or incorrect, will therefore be rejected.
	
All offers are non-binding. The Verein Zentralwäscherei expressly reserves the right to change, supplement or delete parts of the pages or the entire offer without prior notice or to discontinue publication temporarily or permanently.
	
All of the indicated prices are in Swiss francs. Prices are subject to change without notice. Errors in prices or times are excepted.

---

**Copyrights**

The copyrights and all other rights to the content, images, photos or other files on the website belong exclusively to the Verein Zentralwäscherei - inklusive Kultur or to the specifically named rights holders. The written consent of the copyright holders must be obtained in advance for the reproduction of any elements.

---

**Data protection**

Based on Article 13 of the Swiss Federal Constitution and the data protection regulations of the Swiss Confederation (Data Protection Act, DSG), every person has the right to protection of their privacy as well as protection against misuse of their personal data. We comply with these provisions. Personal data is treated as strictly confidential and is neither sold nor passed on to third parties.
	
In close cooperation with our hosting providers, we strive to protect the databases as well as possible against unauthorized access, loss, misuse or falsification.`,
}
