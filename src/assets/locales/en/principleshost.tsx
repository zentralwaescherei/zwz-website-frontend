export default {
	'principleshost.pre': `# ***Please read our principles first.***`,
	'principleshost.after': `# ***[Form: ZW OpenCall Program](https://coda.io/form/ZW-OpenCall-Program-English_dp9gfv2opKe)***`,
	'principleshost.principles': `We are committed to a space with a variety of uses, where people can try out and experiment. 

In particular, people, collectives, initiatives and projects that do this and do not yet have their own permanent place in the city of Zurich should find space in the ZW. 
	
We want to deal constructively with political and social issues through what happens in the space.
	
We do not operate for profit and there is no compulsion to consume. We strive to be self-supporting, without the lack of money for the use of the space being an insurmountable obstacle.
	
Accessibility is one of the core issues of the project. For newcomers who join the principles, access should always be as low-threshold as possible in order to actively participate in shaping the space and its structures. 
	
We want to promote exchange, synergies, collaborations and interactions - between the association, organizers, visitors, neighbors and other interested parties.
	
There should always be room to talk about needs, wishes and differences. Concerns of others should be listened to, as well as own concerns are brought in.
	
We wish for an interaction that is characterized by mutual respect and in which togetherness is the center of attention.
	
We reflect all structures of the project regularly and adjust them if necessary.
	
We create an inviting space in which many things are allowed to happen and which actively seeks the open participation of new people. We create a space of possibility that can also be actively shaped and experienced by visitors.
	
Discrimination, especially in the form of sexism, nationalism, racism and homophobia have no place. Instead, the interaction is characterized by attentiveness and mutual respect.
	
In the association Zentralwäscherei the members and external organizers act with a non-profit oriented attitude - with activities in the Zentralwäscherei no financial/economic enrichment of individuals may take place. 
	
We seek a reflexive and self-critical approach to political and social issues, situations and patterns. We critically reflect on our own power structures and are open to adapt our own actions to these reflections.
	
We strive for diversity in the organization, its structure and the program that emerges from it. We desire diversity in the areas of gender, age, social, cultural and ethnic origin, and sexual orientation. 
	
The Zentralwäscherei should be accessible to all interested persons who agree with our principles. Promoting diversity means taking responsibility and appropriate measures to prevent discrimination and promote recognition of diversity.`,
}
