export default {
	'organisation.content': `We applied to use the open space in the old Zentralwäscherei at Neue Hard 12 in Zurich, which was put out to tender by the city. After the city accepted the bid, we were able to move into the Shedhalle next to the Hardbrücke for the first time as an association in April 2019.  

Our vision is to collectively and self-determinedly create a non-profit cultural and meeting space. This vision requires an organizational structure that provides enough stability for the multi-year project while being flexible enough to remain accessible and open. 
	
The supporting base of the structure is the association. It is open to all natural and legal persons who agree to the common principles. Members can participate in the various organs of the association such as the board, program and working groups. In all organs an equal distribution of genders and disciplinary and social backgrounds is strived for. Because this is a prerequisite for a project that is as inclusive as possible. All members of the association, except the operating group, work on the project on a voluntary basis. 
	
The **general assembly**, which meets once a month, determines the principles, amends the statutes, delegates tasks and competences to the board, operating group, program groups, reflection council and working groups. 
	
The **board** is elected annually at an ordinary General Assembly and consists of 2 to 8 members. The board keeps the books of the association, organizes the general meetings and represents the association against third parties and contractual partners. The task of the board is to represent the interests of the general assembly and the association as a whole. 
	
The **operating group** is responsible for a smooth daily operation. It is divided into the expertise Gastro, Finance, Curation, Graphic, Technic and Care Work. The work in the operating group is remunerated by a standard wage. The distribution of the available and to be determined job percentages is determined by the board.
	
The **working groups** consist of members who work together on a specific topic. These groups can be constituted for a short period of time or can last for the entire duration of the project. At the plenary meetings, the respective working groups are given room for maneuver in which they can act autonomously. Thematically, the spectrum ranges from information technology to diversity and curation to exterior design. They enable the members of the association to participate as low-threshold as possible. 
	
All work within the bodies is documented in a digital platform to which all members have access. In this way, we ensure low-threshold and fast information flow and access.  
	
Would you like to join?`,
}
