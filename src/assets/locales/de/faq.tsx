export default {
	'faq.content': `- **Wie finanziert ihr euch?**
- Mieterlass der Stadt, Baukredit erwähnen (und Gemeinderatsbeschluss verlinken), dann Stiftungen erwähnen und nicht zuletzt sagen, dass mensch uns jederzeit spenden kann.
---
- **Wie viel Miete bezahlt ihr?**
- Nein, wir bezahlen aufgrund des zuvor erwähnten Gemeinderatsbeschlusses keine Miete. Dies erlaubt uns, den Raum und die Gastronomie zu möglichst günstigen Konditionen anzubieten. Wir sind bestrebt, und gemäss unseren Statuten auch verpflichtet, allfällige Gewinne wieder ins Projekt zu investieren. Sollten wir dennoch Gewinn abwerfen, müssten wir der Stadt eine Miete bezahlen.
---
- **Wo finde ich eure Statuten?**
- Unsere Statuten findest du hier [hier](/transparency). Zusätzlich sind wir im [Handelsregister]() eingetragen.
---
- **Kann ich eure Location für meine Hochzeit reservieren?**
- Nein, leider nicht. Alle unsere Veranstaltungen müssen einen öffentlichen Charakter besitzen, es sei denn, sie betreffen Gesellschaftsgruppen, für die ein geschützter Rahmen geschafft werden muss. Weitere Infos zu unsere Programmation findest du [hier](/host).
---
- **Was ist euer Verhältnis zur Stadt?**
- Wir betreiben den Raum und übernehmen den Raum in einer Gebrauchsleihe von der Raumbörse/Stadt Zürich.  Die Stadt Zürich unterstützt uns per Mieterlass und Baukredit (GR-Beschluss). Ansonsten sind wir als Verein unabhängig von der Stadt.
---
---
Bei weiteren Fragen nimm gerne mit und [Kontakt](/contact) auf.`,
}
