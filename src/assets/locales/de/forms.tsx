export default {
	'form.name': 'NAME',
	'form.name-awareness': 'NAME - OPTIONAL',
	'form.email': 'EMAIL',
	'form.email-awareness': 'EMAIL - OPTIONAL',
	'form.message': 'NACHRICHT',
	'form.submit': 'ABSENDEN',
	'form.sucess': 'Erfolgreich abgesendet.',
}
