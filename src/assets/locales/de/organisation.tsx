export default {
	'organisation.content': `Wir bewarben uns für die Bespielung des von der Stadt ausgeschriebenen Freiraums in der alten Zentralwäscherei  an der Neuen Hard 12 in Zürich. Nach dem Zuschlag seitens der Stadt konnten wir im April 2019 zum ersten Mal als Verein in die Shedhalle neben der Hardbrücke. 

Unsere Vision ist es, kollektiv und selbstbestimmt einen nicht gewinnorientierten Kultur- und Begegnungsraum zu schaffen. Diese Vision erfordert eine Organisationsstruktur, die genügend Stabilität für das mehrjährige Projekt bietet und gleichzeitig genügend flexibel ist, um zugänglich und offen bleiben. 
	
Die tragende Basis der Struktur ist der Verein. Er ist offen für alle natürlichen und juristischen Personen, die den gemeinsamen Grundsätzen zustimmen. Mitglieder können sich in den verschiedenen Organen des Vereins wie dem Vorstand, Programm- und Arbeitsgruppen einbringen. Es wird in allen Organen eine möglichst gleichmässige Verteilung der Geschlechter und der disziplinären und sozialen Hintergründen angestrebt. Denn diese ist Voraussetzung für ein möglichst inklusives Projekt. Alle Vereinsmitglieder, ausser die Betriebsgruppe, arbeiten ehrenamtlich am Projekt mit. 
	
Die **Vollversammlung**, die einmal pro Monat stattfindet, legt die Grundsätze fest, ändert die Statuten, delegiert Aufgaben und Kompetenzen an den Vorstand, Betriebsgruppe, Programmgruppen, Reflexionsrat und Arbeitsgruppen. 
	
Der **Vorstand** wird jährlich an einer ordentlichen Vollversammlung gewählt und besteht aus 2 bis 8 Mitgliedern. Der Vorstand führt die Geschäftsbücher des Vereins, organisiert die Vollversammlungen und vertritt den Verein gegen gegenüber Dritten und Vertragspartner*innen. Die Aufgabe des Vorstands ist es, die Interessen der Vollversammlung und des gesamten Vereins zu vertreten. 
	
Die **Betriebsgruppe** ist verantwortlich für einen reibungslosen betrieblichen Alltag. Sie ist in die Expertisen Gastro, Finanzen, Kuration, Grafik, Technik und Carearbeit unterteilt. Die Arbeit in der Betriebsgruppe wird per Einheitslohn entlohnt. Die Verteilung der zu bestimmenden und Verfügung stehenden Stellenprozenten wird vom Vorstand festgelegt.
	
Die **Arbeitsgruppen** bestehen aus Mitgliedern, die zu einem konkreten Thema zusammenarbeiten. Diese Gruppen können sich kurzweilig konstituieren oder aber über die gesamte Projektdauer Bestand haben. An den Vollversammlungen wird den jeweiligen Arbeitsgruppen ein Handlungsspielraum definiert, in welchem sie autonom agieren können. Thematisch reicht das Spektrum von Informatik über Diversität und Kuration zu Aussenraumgestaltung. Sie ermöglichen Vereinsmitgliedern eine möglichst niederschwellige Partizipationsmöglichkeit. 
	
Die gesamte Arbeit innerhalb der Organe wird in einer digitalen Plattform dokumentiert, zu der alle Mitglieder Zugang haben. So gewährleisten wir niederschwelligen und schnellen Informationsfluss und -zugang.  
	
Möchtest du mitmachen?`,
}
