export default {
	'covid.banner': `Immersive  3D-AUDIO FESTIVAL 3.-7. MAI 2022`,
	'covid.link': `Covid Info`,
	'covid.content': `**Ab dem 17. Februar 2022 ist die Masken- und Zertifikatspflicht in der ZW aufgehoben.**

---
- **Denk daran:**
- • Geimpfte Personen können Covid-19 immer noch verbreiten - der Schutz vor Übertragbarkeit nimmt nach 2-3 Monaten nach der vollständigen Impfung deutlich ab
- • Covid-19 kann zu schweren Erkrankungen führen

---
- **Verringerung der Verbreitung von Covid-19:**
- • Wasch und desinfizier dir bitte regelmässig und gründlich die Hände
- • Wenn du krank bist oder Symptome hast, bleib bitte zu Hause und mach einen Selbsttest (auch wenn du geimpft bist)
- • Halte nach Möglichkeit Abstand (in der Schlange, an der Bar usw.).`,
}
