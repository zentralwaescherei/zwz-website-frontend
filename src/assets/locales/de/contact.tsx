export default {
	'contact.content': `Gerne kannst du uns bei Fragen via Mail kontaktieren. Auch Anregungen und Feedbacks sind jederzeit willkommen!

- Allgemeine Anfragen an den Verein bitte an:
- [info@zentralwaescherei.space](mailto:info@zentralwaescherei.space)
---
- Du willst aktiv beim Verein mitwirken? Dann melde dich bitte hier: 
- [mitmachen@zentralwaescherei.space](mailto:mitmachen@zentralwaescherei.space?subject=Mitmachen)
---
- Für Anfragen bezüglich Programmation melde dich bitte hier: 
- [programm@zentralwaescherei.space](mailto:programm@zentralwaescherei.space?subject=Programm)
---
- Für Anfragen bezüglich Gastronomie: 
- [gastro@zentralwaescherei.space](mailto:gastro@zentralwaescherei.space?subject=Gastro)
---
- Verein Zentralwäscherei
- Neue Hard 12
- 8005 Zürich`,
}
