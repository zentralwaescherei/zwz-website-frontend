export default {
	'awareness.content': `Awareness bedeutet - Bewusstsein. Wir als Verein Zentralwäscherei möchten bewusst unterwegs sein. Ob Vereinsmitglieder, auftretenden Künstler*innen oder Besuchende, es sollte uns allen jederzeit bewusst sein, was unsere Handlungen bei anderen Menschen auslösen können. Damit gemeint sind Aussagen, welche sexistisch, rassistisch, ableistisch, homo- oder transfeindlich sind, sowie auch Belästigungen jeglicher Art.

Wir respektieren die Grenzen unserer Mitmenschen sowie auch unsere eigenen. Oft werden wir von Mustern begleitet, welche nicht einfach abzulegen sind. Trotzdem erwarten wir, dass sich alle, welche sich in den Räumlichkeiten des Vereins Zentralwäscherei aufhalten, aktiv damit auseinandersetzen, Awareness vorzuleben und auch umzusetzen.

---

*Warst du in den Räumen der ZW und hattest ein Erlebnis welches du gerne mit uns teilen oder besprechen möchtest?*

Der digitale Briefkasten ist da für Anregungen, Kritik und persönliche Erfahrungen. Alle Beiträge werden von unserem Awareness-Team gelesen und wenn der Wunsch besteht, folgt eine Kontaktaufnahme. Feedback ist auch anonym möglich. Schreib uns in der Sprache in der du dich am wohlsten fühlst.`,
	'awareness.content-after': `---
- Unsere Awareness-Leitfaden findest du [HIER](https://drive.google.com/file/d/1m2mfzj-MTPw9GdHaGCd1q35d8JG37QZz/view?usp=sharing)
- Stand November 2021`,
}
