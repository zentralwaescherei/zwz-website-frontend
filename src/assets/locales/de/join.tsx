export default {
	main: `Interessierte, welche sich über Veranstaltungen hinaus vertieft in den Betrieb der Halle einbringen möchten, können sich als Mitglieder des Vereins bewerben. Bitte fülle dazu die unverbindliche Anfrage aus oder schreibe eine Email an *[mitmachen@zentralwaescherei.space](mailto:mitmachen@zentralwaescherei.space)*`,
	button: 'MITMACHEN',
	buttonClose: 'SCHLIESSEN',
}
