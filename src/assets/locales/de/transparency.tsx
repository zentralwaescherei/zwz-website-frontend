export default {
	'transparency.content': `Wir bemühen uns möglichst alle Aspekte des Vereins öffentlich und transparent zu halten. Auf dieser Seite findest du einen Einblick in viele Bereiche des Vereins. Falls du noch mehr erfahren willst, melde dich gerne als Neumitglied an.
---
---
- [Vereinsstatuten](https://drive.google.com/file/d/1NeLW_cLsAVfm1GFMcxcb-C3xNxp5NEg2/view?usp=sharing)
- Hier kannst du die Statuten herunterladen.
---
- [Leistungsvereinbarung Stadt Zürich](https://drive.google.com/file/d/1ak_8XvJy9phHKY6lXNIQvgQdv5o3xl3_/view?usp=sharing)
- Hier kannst du unsere Leistungsvereinbarung herunterladen.
---
- [Bewerbung des Vereins für diese Räumlichkeiten](https://drive.google.com/file/d/1wL9AOTWOnD0QZlq2VZZq__UZz_QvNmg7/view?usp=sharing)
- Hier findest du unser Bewerbungsdossier.
---
- [Kurationsrichtlinien](https://docs.google.com/document/d/1B9hpZNoLfe9l17VjccezID-IeZKqeRWNfXvuKRLUB_o/edit#heading=h.boxkmdjs9i0b)
- Hier kannst du unsere Kurationsrichtlinien herunterladen.`,
}
