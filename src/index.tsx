import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { App } from './app'
import { store } from './app/store'
import { Provider } from 'react-redux'

import './i18n'

ReactDOM.render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById('root')
)
