import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'

import generalDE from './assets/locales/de/general'
import generalEN from './assets/locales/en/general'

import joinDE from './assets/locales/de/join'
import joinEN from './assets/locales/en/join'

import aboutDE from './assets/locales/de/about'
import aboutEN from './assets/locales/en/about'

import contactDE from './assets/locales/de/contact'
import contactEN from './assets/locales/en/contact'

import eventDE from './assets/locales/de/event'
import eventEN from './assets/locales/en/event'

import awarenessDE from './assets/locales/de/awareness'
import awarenessEN from './assets/locales/en/awareness'

import hostDE from './assets/locales/de/host'
import hostEN from './assets/locales/en/host'

import imprintDE from './assets/locales/de/imprint'
import imprintEN from './assets/locales/en/imprint'

import imprintOverlayDE from './assets/locales/de/imprint-overlay'
import imprintOverlayEN from './assets/locales/en/imprint-overlay'

import jobsDE from './assets/locales/de/jobs'
import jobsEN from './assets/locales/en/jobs'

import organisationDE from './assets/locales/de/organisation'
import organisationEN from './assets/locales/en/organisation'

import principlesDE from './assets/locales/de/principles'
import principlesEN from './assets/locales/en/principles'

import transparencyDE from './assets/locales/de/transparency'
import transparencyEN from './assets/locales/en/transparency'

import navigationEN from './assets/locales/en/navigation'
import navigationDE from './assets/locales/de/navigation'

import formsEN from './assets/locales/en/forms'
import formsDE from './assets/locales/de/forms'

import openingDE from './assets/locales/de/opening'
import openingEN from './assets/locales/en/opening'

import faqEN from './assets/locales/en/faq'
import faqDE from './assets/locales/de/faq'

import filterEN from './assets/locales/en/filter'
import filterDE from './assets/locales/de/filter'

import gastroDE from './assets/locales/de/gastro'
import gastroEN from './assets/locales/en/gastro'

import workshopDE from './assets/locales/de/workshop'
import workshopEN from './assets/locales/en/workshop'

import covidDE from './assets/locales/de/covid'
import covidEN from './assets/locales/en/covid'

import principleshostDE from './assets/locales/de/principleshost'
import principleshostEN from './assets/locales/en/principleshost'

// the translations
const resources = {
	en: {
		general: generalEN,
		about: aboutEN,
		contact: contactEN,
		event: eventEN,
		awareness: awarenessEN,
		host: hostEN,
		imprint: imprintEN,
		imprintOverlay: imprintOverlayEN,
		jobs: jobsEN,
		join: joinEN,
		organisation: organisationEN,
		principles: principlesEN,
		transparency: transparencyEN,
		navigation: navigationEN,
		forms: formsEN,
		opening: openingEN,
		faq: faqEN,
		filter: filterEN,
		gastro: gastroEN,
		workshop: workshopEN,
		covid: covidEN,
		principleshost: principleshostEN,
	},
	de: {
		general: generalDE,
		join: joinDE,
		about: aboutDE,
		contact: contactDE,
		event: eventDE,
		awareness: awarenessDE,
		host: hostDE,
		imprint: imprintDE,
		imprintOverlay: imprintOverlayDE,
		jobs: jobsDE,
		organisation: organisationDE,
		principles: principlesDE,
		transparency: transparencyDE,
		navigation: navigationDE,
		forms: formsDE,
		opening: openingDE,
		faq: faqDE,
		filter: filterDE,
		gastro: gastroDE,
		workshop: workshopDE,
		covid: covidDE,
		principleshost: principleshostDE,
	},
}

i18n
	.use(initReactI18next) // passes i18n down to react-i18next
	.init({
		resources,
		lng: 'de',
		fallbackLng: 'de',
		keySeparator: false, // we do not use keys in form messages.welcome
		interpolation: { escapeValue: false }, // react already safes from xss
	})

export default i18n
