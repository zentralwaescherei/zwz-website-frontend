// Or from '@reduxjs/toolkit/query/react'
import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { baseUrlApi } from './base'
import { JoinRequest } from './join.routes'

export const joinApi = createApi({
	reducerPath: 'joinApi',
	baseQuery: fetchBaseQuery({ baseUrl: baseUrlApi }),
	endpoints: (builder) => ({
		createJoinRequest: builder.mutation<JoinRequest, JoinRequest>({
			query: (data: JoinRequest) => ({
				url: '/join',
				method: 'post',
				body: data,
			}),
		}),
	}),
})

export const { useCreateJoinRequestMutation } = joinApi
