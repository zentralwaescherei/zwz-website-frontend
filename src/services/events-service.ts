import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { baseUrlApi } from './base'
import { Events, EventType } from './events.routes'

type EventFromToProps = { from: string; to: string }
type GetEventByIdArgs = {
	id: string
	lang: string
}

type QueryEventsArgs = {
	lang: string
	from: number
	to: number
}

export const eventsApi = createApi({
	reducerPath: 'eventsApi',
	baseQuery: fetchBaseQuery({ baseUrl: baseUrlApi }),
	endpoints: (builder) => ({
		getEventByDateRange: builder.query<Events, EventFromToProps>({
			query: ({ from, to }) => `events?from=${from}&to=${to}`,
		}),
		getEventById: builder.query<EventType, GetEventByIdArgs>({ query: ({ id, lang }) => `events/${id}?lang=${lang}` }),
		getEvents: builder.query<Events, QueryEventsArgs>({
			query: ({ lang, from, to }) => {
				let url = `events/?lang=${lang}&direction=${'ascending'}`
				if (from > 0) url += `&from=${from}`
				if (to > 0) url += `&to=${to}`
				return url
			},
		}),
	}),
})

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const { useGetEventByIdQuery, useGetEventsQuery } = eventsApi
