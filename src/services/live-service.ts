import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { baseUrlApi } from './base'

type LiveState = {
	isLive: boolean
}

export const liveApi = createApi({
	reducerPath: 'liveApi',
	baseQuery: fetchBaseQuery({ baseUrl: baseUrlApi }),
	endpoints: (builder) => ({
		getStreaming: builder.query<LiveState, void>({
			query: () => 'streaming',
		}),
	}),
})

export const { useGetStreamingQuery } = liveApi
