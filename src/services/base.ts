const zwUrl = process.env.ZW_URL
export const baseUrl = process.env.NODE_ENV === 'development' ? (zwUrl ? `${zwUrl}` : 'http://localhost:8081') : ''
export const baseUrlApi = process.env.NODE_ENV === 'development' ? `${baseUrl}/api/` : '/api/'
