import * as React from 'react'
import { Navigation } from './features/navigation/navigation'
import { BrowserRouter as Router, Route } from 'react-router-dom'

import './style.scss'
import { Index } from './features/index'
import { Opening } from './features/opening/opening'
import { About } from './features/about/about'
import { Logo } from './features/logo/logo'
import { Join } from './features/join/join'
import { Awareness } from './features/awareness/awareness'
import { Contact } from './features/contact/contact'
import { Host } from './features/host/host'
import { Imprint } from './features/imprint/imprint'
import { Jobs } from './features/jobs/jobs'
import { Organisation } from './features/organisation/organisation'
import { Principles } from './features/principles/principles'
import { Calendar } from './features/calendar/calendar'
import { Transparency } from './features/transparency/transparency'
import { ZwOnline } from './features/zw-www/zw-www'
import { ImprintOverlay } from './features/imprint-overlay/imprint-overlay'
import { EventPage } from './features/events/event-page'
import { Gastro } from './features/gastro/gastro'
import { Faq } from './features/faq/faq'
import { TopBorder } from './features/topborder/topborder'
import { Workshop } from './features/workshop/workshop'
import { Live } from './features/live/live'
import { Covid } from './features/covid/covid'
import { Menu } from './features/menu/menu'
import { PrinciplesHost } from './features/principleshost/principleshost'
import { Immersive } from './features/immersive/immersive'

export const App = (): JSX.Element => {
	return (
		<React.Suspense fallback="loading">
			<Router>
				<Logo></Logo>
				<TopBorder></TopBorder>
				<Join></Join>
				<ImprintOverlay></ImprintOverlay>
				<div className="app-container">
					<Navigation />
					<Route exact path="/">
						<Index />
					</Route>
					<Route path="/opening">
						<Opening />
					</Route>
					<Route path="/calendar">
						<Calendar />
					</Route>
					<Route path="/event/:id">
						<EventPage />
					</Route>
					<Route path="/about">
						<About />
					</Route>
					<Route path="/awareness">
						<Awareness />
					</Route>
					<Route path="/contact">
						<Contact />
					</Route>
					<Route path="/host">
						<Host />
					</Route>
					<Route path="/imprint">
						<Imprint />
					</Route>
					<Route path="/jobs">
						<Jobs />
					</Route>
					<Route path="/organisation">
						<Organisation />
					</Route>
					<Route path="/principles">
						<Principles />
					</Route>
					<Route path="/transparency">
						<Transparency />
					</Route>
					<Route path="/zw-www">
						<ZwOnline />
					</Route>
					<Route path="/gastro">
						<Gastro />
					</Route>
					<Route path="/faq">
						<Faq />
					</Route>
					<Route path="/workshop">
						<Workshop />
					</Route>
					<Route path="/live">
						<Live />
					</Route>
					<Route path="/menu">
						<Menu />
					</Route>
					<Route path="/covid">
						<Covid />
					</Route>
					<Route path="/immersive">
						<Immersive />
					</Route>
					<Route path="/principles-host">
						<PrinciplesHost />
					</Route>
				</div>
			</Router>
		</React.Suspense>
	)
}
