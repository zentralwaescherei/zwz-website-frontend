# ZW Frontend

## Development

Use the `ZW_URL` environment variable to specify backend for development.

```bash
ZW_URL=https://zentralwaescherei.space npm start
```

or

```
npm run dev
```

which does the equivalent thing.

The frontend expects that the API is accessible via `{ZW_URL}/api`